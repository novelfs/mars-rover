package com.itv.rover

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class GridSpec extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks with DomainArbitraries {
  "plains of n size" should "consist entirely of plains" in {
    forAll { nByte: Byte =>
      val n = math.abs(nByte.toInt)
      val grid = Grid.plains(n)
      val items = for(i <- 0 until n; j <- 0 until n) yield grid.get(i)(j)
      items.forall {
        case Some(GridItem.Plains) => true
        case _ => false
      } shouldBe true
    }
  }

  "plains of n size" should "have width of n" in {
    forAll { nByte: Byte =>
      val n = math.abs(nByte.toInt)
      val grid = Grid.plains(n)
      grid.width shouldBe n
    }
  }

  "plains of n size" should "have height of n" in {
    forAll { nByte: Byte =>
      val n = math.abs(nByte.toInt)
      val grid = Grid.plains(n)
      grid.height shouldBe n
    }
  }
}

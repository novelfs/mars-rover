package com.itv.rover

import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FlatSpec, Matchers}

class FacingSpec extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks with DomainArbitraries {

  "rotateAnticlockwise" should "generate correct facing" in {
    forAll { facing: Facing =>
      val expectedNewFacing = facing match {
        case Facing.Up    => Facing.Left
        case Facing.Left  => Facing.Down
        case Facing.Down  => Facing.Right
        case Facing.Right => Facing.Up
      }

      expectedNewFacing shouldBe facing.nextAnticlockwise
    }
  }

  "rotateClockwise" should "generate correct facing" in {
    forAll { facing: Facing =>
      val expectedNewFacing = facing match {
        case Facing.Up    => Facing.Right
        case Facing.Left  => Facing.Up
        case Facing.Down  => Facing.Left
        case Facing.Right => Facing.Down
      }

      expectedNewFacing shouldBe facing.nextClockwise
    }
  }

}

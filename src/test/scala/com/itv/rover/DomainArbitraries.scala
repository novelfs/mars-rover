package com.itv.rover

import org.scalacheck.{Arbitrary, Gen}

trait DomainArbitraries {
  implicit val facingArb: Arbitrary[Facing] = Arbitrary(Gen.oneOf(List(Facing.Up, Facing.Down, Facing.Left, Facing.Right)))
}

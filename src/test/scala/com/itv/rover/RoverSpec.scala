package com.itv.rover

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class RoverSpec extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks with DomainArbitraries {

  "moveForward" should "move the rover up when facing up" in {
    forAll { j : Int =>
      val actualRover = Rover(Facing.Up, 0, j).moveForward
      val expectedRover = Rover(Facing.Up, 0, j - 1)
      actualRover shouldBe expectedRover
    }
  }

  it should "move the rover down when facing down" in {
    forAll { j : Int =>
      val actualRover = Rover(Facing.Down, 0, j).moveForward
      val expectedRover = Rover(Facing.Down, 0, j + 1)
      actualRover shouldBe expectedRover
    }
  }

  it should "move the rover left when facing left" in {
    forAll { i: Int =>
      val actualRover = Rover(Facing.Left, i, 0).moveForward
      val expectedRover = Rover(Facing.Left, i - 1, 0)
      actualRover shouldBe expectedRover
    }
  }

  it should "move the rover right when facing right" in {
    forAll { i: Int =>
      val actualRover = Rover(Facing.Right, i, 0).moveForward
      val expectedRover = Rover(Facing.Right, i + 1, 0)
      actualRover shouldBe expectedRover
    }
  }

  "yOnGrid" should "wrap around when the rover position would become negative" in {
    val grid = Grid.plains(4)
    val newY = Rover(Facing.Up, 0, 0).moveForward.yOnGrid(grid)
    newY shouldBe 3
  }

  it should "wrap around when the rover position would become equal to the grid height" in {
    val grid = Grid.plains(4)
    val newY = Rover(Facing.Down, 0, 3).moveForward.yOnGrid(grid)
    newY shouldBe 0
  }

  "xOnGrid" should "wrap around when the rover position would become negative" in {
    val grid = Grid.plains(4)
    val newX = Rover(Facing.Left, 0, 0).moveForward.xOnGrid(grid)
    newX shouldBe 3
  }

  it should "wrap around when the rover position would become equal to the grid width" in {
    val grid = Grid.plains(4)
    val newX = Rover(Facing.Right, 3, 0).moveForward.xOnGrid(grid)
    newX shouldBe 0
  }
}

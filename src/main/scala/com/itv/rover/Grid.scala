package com.itv.rover

final case class Grid private (private val rawGrid : Array[Array[GridItem]])

object Grid {
  def plains(n : Int): Grid = Grid(Array.fill[GridItem](n, n)(GridItem.Plains.asInstanceOf[GridItem]))

  implicit class GridOps(val grid : Grid) extends AnyVal {
    def width: Int = grid.rawGrid.length

    def height: Int = grid.rawGrid(0).length

    def get(x : Int)(y : Int) : Option[GridItem] =
      if (x < grid.width && y < grid.height)
        Some(grid.rawGrid(x)(y))
      else
        None
  }
}
package com.itv.rover

sealed trait Facing

object Facing {
  final case object Up extends Facing
  final case object Down extends Facing
  final case object Left extends Facing
  final case object Right extends Facing

  implicit class FacingOps(val facing : Facing) extends AnyVal {
    def nextAnticlockwise: Facing = facing match {
      case Up    => Left
      case Left  => Down
      case Down  => Right
      case Right => Up
    }

    def nextClockwise: Facing = facing match {
      case Up    => Right
      case Right => Down
      case Down  => Left
      case Left  => Up
    }
  }
}

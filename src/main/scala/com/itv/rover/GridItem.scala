package com.itv.rover

sealed trait GridItem

object GridItem {
  final case object Plains extends GridItem
  final case object Mountains extends GridItem
}

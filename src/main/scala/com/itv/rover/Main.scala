package com.itv.rover

import cats.data.{ReaderT, StateT}
import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import org.novelfs.pure.log.simple._
import cats.mtl.implicits._
import com.itv.rover.effects.rovercommand.MonadRoverCommand
import com.itv.rover.effects.rovercommand.state._

object Main extends IOApp {
  type RoverApp[A] = ReaderT[StateT[IO, Rover, ?], Grid, A]

  val roverOps: RoverApp[Unit] = for {
    _ <- MonadRoverCommand[RoverApp].rotateAnticlockwise
    _ <- MonadRoverCommand[RoverApp].moveForward
    _ <- MonadRoverCommand[RoverApp].rotateAnticlockwise
    _ <- MonadRoverCommand[RoverApp].moveForward
    _ <- MonadRoverCommand[RoverApp].moveForward
    _ <- MonadRoverCommand[RoverApp].rotateClockwise
    _ <- MonadRoverCommand[RoverApp].moveForward
    _ <- MonadRoverCommand[RoverApp].moveForward
    _ <- MonadRoverCommand[RoverApp].moveForward
    _ <- MonadRoverCommand[RoverApp].moveForward
    _ <- MonadRoverCommand[RoverApp].moveForward
  } yield ()

  override def run(args: List[String]): IO[ExitCode] = {
    val grid = Grid.plains(5)
    val initialRoverState = Rover(Facing.Up, 0, 0)
    roverOps
      .run(grid)
      .run(initialRoverState)
      .map(_ => ExitCode.Success)
  }
}

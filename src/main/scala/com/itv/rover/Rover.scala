package com.itv.rover

final case class Rover private (facing : Facing, private val x : Int, private val y : Int)

object Rover {
  implicit class RoverOps(val rover: Rover) extends AnyVal {
    def moveForward : Rover = rover.facing match {
      case Facing.Up    => rover.copy(y = rover.y - 1)
      case Facing.Down  => rover.copy(y = rover.y + 1)
      case Facing.Left  => rover.copy(x = rover.x - 1)
      case Facing.Right => rover.copy(x = rover.x + 1)
    }

    def positionOnGrid(grid : Grid): (Int, Int) = (xOnGrid(grid), yOnGrid(grid))

    def xOnGrid(grid: Grid): Int = Math.floorMod(rover.x, grid.width)

    def yOnGrid(grid: Grid): Int = Math.floorMod(rover.y, grid.height)
  }
}

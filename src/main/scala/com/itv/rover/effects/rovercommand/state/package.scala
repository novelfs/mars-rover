package com.itv.rover.effects.rovercommand

import cats.Monad
import cats.mtl.{ApplicativeAsk, MonadState}
import com.itv.rover.{Grid, Rover}
import cats.implicits._
import org.novelfs.pure.log.{LogLevel, MonadLogger}

package object state {
  implicit def stateMonadRoverCommand[F[_] : Monad : MonadState[?[_], Rover] : ApplicativeAsk[?[_], Grid] : MonadLogger] : MonadRoverCommand[F] = new MonadRoverCommand[F] {
    override def monad : Monad[F] = Monad[F]

    override def rotateAnticlockwise: F[Unit] =
      for {
        oldRoverState <- MonadState[F, Rover].get
        newRoverState = oldRoverState.copy(facing = oldRoverState.facing.nextAnticlockwise)
        _ <- MonadState[F, Rover].set(newRoverState)
        grid <- ApplicativeAsk[F, Grid].ask
        _ <- MonadLogger[F].log(LogLevel.Info)(s"Rover is now facing ${newRoverState.facing} at x : ${newRoverState.xOnGrid(grid)} and y : ${newRoverState.yOnGrid(grid)}")
      } yield ()

    override def rotateClockwise: F[Unit] =
      for {
        oldRoverState <- MonadState[F, Rover].get
        newRoverState = oldRoverState.copy(facing = oldRoverState.facing.nextClockwise)
        _ <- MonadState[F, Rover].set(newRoverState)
        grid <- ApplicativeAsk[F, Grid].ask
        _ <- MonadLogger[F].log(LogLevel.Info)(s"Rover is now facing ${newRoverState.facing} at x : ${newRoverState.xOnGrid(grid)} and y : ${newRoverState.yOnGrid(grid)}")
      } yield ()

    override def moveForward: F[Unit] =
      for {
        oldRoverState <- MonadState[F, Rover].get
        newRoverState = oldRoverState.moveForward
        _ <- MonadState[F, Rover].set(newRoverState)
        grid <- ApplicativeAsk[F, Grid].ask
        _ <- MonadLogger[F].log(LogLevel.Info)(s"Rover is now facing ${newRoverState.facing} at x : ${newRoverState.xOnGrid(grid)} and y : ${newRoverState.yOnGrid(grid)}")
      } yield ()
  }
}

package com.itv.rover.effects.rovercommand

import cats.Monad
import simulacrum.typeclass

@typeclass trait MonadRoverCommand[F[_]] {
  def monad : Monad[F]
  def rotateAnticlockwise : F[Unit]
  def rotateClockwise : F[Unit]
  def moveForward : F[Unit]
}
